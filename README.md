<!-- This is a comment in md (Markdown) format, it will not be visible to the end user -->
## _Deprecation Notice_
This Pre-Built has been deprecated as of 08-24-2023 and will be end of life on 06-30-2024. The capabilities of this Pre-Built have been replaced by the [Itential ServiceNow - Now Platform - REST Workflow Project](https://gitlab.com/itentialopensource/pre-built-automations/servicenow-now-platform-rest)

<!-- Update the below line with your Pre-Built name -->
# ServiceNow Incident Creation

<!-- Leave TOC intact unless you've added or removed headers -->
## Table of Contents

- [ServiceNow Incident Creation](#servicenow-incident-creation)
  - [Table of Contents](#table-of-contents)
  - [Overview](#overview)
  - [Installation Prerequisites](#installation-prerequisites)
  - [Requirements](#requirements)
  - [Features](#features)
  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
    - [Input Variables](#input-variables)
  - [Additional Information](#additional-information)

## Overview
This Pre-Built integrates with the ServiceNow Open Source Adapter to create an incident in the `incident` table in ServiceNow.

## Installation Prerequisites

Users must satisfy the following pre-requisites:

- Itential Automation Platform
  - `^2023.1`
- [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow)
  - `^2.2.0`

## Requirements

This Pre-Built requires the following:

- A [ServiceNow Open Source Adapter](https://gitlab.com/itentialopensource/adapters/itsm-testing/adapter-servicenow).
- A ServiceNow account that has write access to the `incident` table.

## Features

The main benefits and features of the Pre-Built are outlined below.
* Provide the fields required to create a ServiceNow Incident.

## How to Install

To install the Pre-Built:

* Verify you are running a supported version of the Itential Automation Platform (IAP) as listed above in the [Requirements](#requirements) section in order to install the Pre-Built. 
* The Pre-Built can be installed from within App-Admin_Essential. Simply search for the name of your desired Pre-Built and click the install button.

## How to Run

Use the following to run the Pre-Built:

* Run the Operations Manager Automation `ServiceNow Incident Creation` or call workflow `ServiceNow Incident Creation` from your workflow in a childJob task.

### Input Variables
_Example_

```json
{
  "incidentSummary": "The summary of the Incident",
  "shortDescription": "The description of the Incident",
  "comments": "Additional comments on the Incident"
}
```

## Additional Information

Please use your Itential Customer Success account if you need support when using this Pre-Built.
