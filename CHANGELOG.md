
## 0.0.23 [01-03-2024]

* Remove img tag from markdown file

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!10

---

## 0.0.22 [09-22-2023]

* add deprecation information

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!9

---

## 0.0.21 [05-26-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!7

---

## 0.0.20 [07-01-2022]

* Certified for 2022.1

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!6

---

## 0.0.19 [12-21-2021]

* Patch/dsup 1046

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!5

---

## 0.0.18 [11-15-2021]

* Update pre-built description

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!3

---

## 0.0.18 [11-15-2021]

* patch/dsup 995

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!4

---

## 0.0.17 [07-09-2021]

* Update package.json, README.md files

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!2

---

## 0.0.16 [05-11-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/servicenow-incident-creation!1

---

## 0.0.15 [05-04-2021]

* Update images/servicenow_incident_creation_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/servicenow-incident-creation!10

---

## 0.0.14 [03-23-2021]

* Update images/servicenow_incident_creation_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/servicenow-incident-creation!10

---

## 0.0.13 [03-01-2021]

* Update images/servicenow_incident_creation_ac_form.png,...

See merge request itential/sales-engineer/selabprebuilts/servicenow-incident-creation!10

---

## 0.0.12 [03-01-2021]

* patch/2021-03-01T15-18-34

See merge request itential/sales-engineer/selabprebuilts/servicenow-incident-creation!9

---

## 0.0.11 [02-26-2021]

* Update README.md

See merge request itential/sales-engineer/selabprebuilts/create-incidentsnow!6

---

## 0.0.10 [02-25-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!3

---

## 0.0.9 [02-24-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!3

---

## 0.0.8 [02-18-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!3

---

## 0.0.7 [02-18-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!3

---

## 0.0.6 [02-18-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!3

---

## 0.0.5 [02-18-2021]

* Update README.md

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!3

---

## 0.0.4 [02-18-2021]

* Jerry.itential master patch 74087

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!2

---

## 0.0.3 [02-17-2021]

* Update package.json

See merge request itential/sales-engineer/selabdemos/create-incidentsnow!1

---

## 0.0.2 [02-17-2021]

* Bug fixes and performance improvements

See commit f3960ab

---\n\n\n
