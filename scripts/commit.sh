git_setup
git checkout "$CI_COMMIT_REF_NAME"
git reset --hard origin/"$CI_COMMIT_REF_NAME"
echo "GITLAB PROJECT ID IS - $CI_PROJECT_ID"
cd scripts
GENERATE=`node generate $CI_PROJECT_ID $CI_PROJECT_URL $CI_COMMIT_REF_NAME`
if [ $? -ne 0 ]
then 
  echo $GENERATE
  exit 1
fi
cd ..
git add artifact.json
git diff --cached --name-only | if grep artifact.json
then
    git commit -m "AUTO-GENERATED FILE [skip ci]"
    git push origin "$CI_COMMIT_REF_NAME"
    echo "A new generated artifact.json file has been checked in to current branch"
else
    echo "artifact.json file hasn't changed"
fi